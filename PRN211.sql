﻿/** Delete DB***/
SET NOCOUNT ON
GO

USE [master]
GO

IF EXISTS (SELECT * FROM sysdatabases WHERE name='PRN211')
		DROP DATABASE PRN211
GO

/** Create DB***/

CREATE DATABASE PRN211
GO

USE [PRN211]
GO
----------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](30) NULL,
	[Password] [nvarchar](30) NULL,
	[FullName] [nvarchar](30) NULL,
	[Gender] [bit] NULL,
	[DOB] [smalldatetime] NULL,
	[Address] [nvarchar](100) NULL,
	[Mobile] [nvarchar](10) NULL,
	[Avatar] [nvarchar](max) NULL,
	[RoleID] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[User] ON
 INSERT INTO [User] ([UserId], [UserName], [Password], [FullName], [Gender], [DOB], [Address], [Mobile], [RoleID])
 VALUES (1, N'thinhhiep2001', N'hiep11052001', 'Nguyen Thinh Hiep', 1, N'2001-11-05', N'Ha Noi','0886135901',1)
 INSERT INTO [User] ([UserId], [UserName], [Password], [FullName], [Gender], [DOB], [Address], [Mobile], [RoleID])
 VALUES (2, N'thinhhiep2001', N'hiep11052001', 'Nguyen Thinh Hiep', 1, N'2001-11-05', N'Ha Noi','0886135901',1)
 INSERT INTO [User] ([UserId], [UserName], [Password], [FullName], [Gender], [DOB], [Address], [Mobile], [RoleID])
 VALUES (3, N'thinhhiep2001', N'hiep11052001', 'Nguyen Thinh Hiep', 1, N'2001-11-05', N'Ha Noi','0886135901',1)
 INSERT INTO [User] ([UserId], [UserName], [Password], [FullName], [Gender], [DOB], [Address], [Mobile], [RoleID])
 VALUES (4, N'thinhhiep2001', N'hiep11052001', 'Nguyen Thinh Hiep', 1, N'2001-11-05', N'Ha Noi','0886135901',1)
SET IDENTITY_INSERT [dbo].[User] OFF

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](40) NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (1, N'Burgers')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (2, N'Pizza')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (3, N'Sallad')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (4, N'Bánh Mì')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (5, N'Chicken')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (6, N'Đồ Ăn Vặt')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (7, N'Nước Ép Trái Cây')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (8, N'Cà Phê')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (9, N'Trà Sữa')
 INSERT INTO [Category] ([CategoryID], [CategoryName]) VALUES (10, N'Kem')
SET IDENTITY_INSERT [dbo].[Category] OFF

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table](
	[TableId] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](10) NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TableId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Table] ON
DECLARE @i int = 1
WHILE @i <= 40
BEGIN
	INSERT INTO [dbo].[Table] ([TableId], [TableName], [Status]) VALUES (@i, CAST(@i as nvarchar(10)), 0)
	SET @i = @i + 1
END
SET IDENTITY_INSERT [dbo].[Table] OFF

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Food](
	[FoodID] [int] IDENTITY(1,1) NOT NULL,
	[FoodName] [nvarchar](100) NULL,
	[CategoryID] [int] NULL,
	CONSTRAINT FK_CATEGORY FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category]([CategoryID]),
PRIMARY KEY CLUSTERED 
(
	[FoodID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Food] ON
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (1, N'PIZZA LẠP XƯỞNG XỐT TRỨNG MUỐI MAYONNAISE - SAIGON MANIA PIZZA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (2, N'PIZZA BÒ & TÔM NƯỚNG KIỂU MỸ - SURF & TURF', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (3, N'PIZZA BÁNH XÈO NHẬT - OKONOMIYAKI', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (4, N'PIZZA THẬP CẨM THƯỢNG HẠNG - EXTRAVAGANZA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (5, N'PIZZA HẢI SẢN XỐT MAYONNAISE - OCEAN MANIA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (6, N'PIZZA HẢI SẢN NHIỆT ĐỚI XỐT TIÊU - PIZZAMIN SEA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (7, N'HALF HALF', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (8, N'PIZZA 5 LOẠI THỊT THƯỢNG HẠNG - MEAT LOVERS', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (9, N'PIZZA XÚC XÍCH Ý TRUYỀN THỐNG - PEPPERONI FEAST', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (10, N'PIZZA HẢI SẢN XỐT CÀ CHUA - SEAFOOD DELIGHT', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (11, N'PIZZA TRỨNG CÚT XỐT PHÔ MAI - KID MANIA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (12, N'PIZZA RAU CỦ THẬP CẨM - VEGGIE MANIA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (13, N'PIZZA GÀ XỐT TƯƠNG KIỂU NHẬT - TERIYAKI CHICKEN', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (14, N'PIZZA DĂM BÔNG DỨA KIỂU HAWAII - HAWAIIAN', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (15, N'PIZZA PHÔ MAI HẢO HẠNG - CHEESE MANIA', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (16, N'PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI - CHEESY CHICKEN BACON', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (17, N'PIZZA BÒ MÊ-HI-CÔ THƯỢNG HẠNG - PRIME BEEF', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (18, N'PIZZA HẢI SẢN KIỂU SINGAPORE - SINGAPORE STYLE SEAFOOD', 2)
 INSERT INTO [dbo].[Food] ([FoodID], [FoodName], [CategoryID])
 VALUES (19, N'PIZZA 4 VỊ - PIZZA BIG 4', 2)
SET IDENTITY_INSERT [dbo].[Food] OFF

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Option](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FoodID] [int] NOT NULL,
	[Size] [char](1) NULL,
	[Price] [int] NULL,
	[Status] [bit] NULL,
	CONSTRAINT FK_FOOD_1 FOREIGN KEY ([FoodID]) REFERENCES [dbo].[Food]([FoodID]),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
DECLARE @i int = 1
WHILE @i <= 10
BEGIN
	IF(@i<=10)
		BEGIN
			INSERT INTO [dbo].[Option] ([FoodID], [Size], [Price], [Status]) VALUES (@i, 'S', 99000, 1)
			INSERT INTO [dbo].[Option] ([FoodID], [Size], [Price], [Status]) VALUES (@i, 'M', 189000, 1)
			INSERT INTO [dbo].[Option] ([FoodID], [Size], [Price], [Status]) VALUES (@i, 'L', 279000, 1)
		END
	ELSE IF(@i<=18)
		BEGIN
			INSERT INTO [dbo].[Option] ([FoodID], [Size], [Price], [Status]) VALUES (@i, 'M', 249000, 1)
			INSERT INTO [dbo].[Option] ([FoodID], [Size], [Price], [Status]) VALUES (@i, 'L', 329000, 1)
		END
	ELSE
		INSERT INTO [dbo].[Option] ([FoodID], [Size], [Price], [Status]) VALUES (@i, 'L', 329000, 1)
	SET @i = @i + 1
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[BillID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NULL,
	[CheckOut] [smalldatetime] NULL,
	[TotalPrice] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT FK_TABLE FOREIGN KEY ([TableID]) REFERENCES [dbo].[Table]([TableID]),
PRIMARY KEY CLUSTERED 
(
	[BillID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BillID] [int] NOT NULL,
	[FoodID] [int] NULL,
	[Size] [char](1) NULL,
	[Amount] [TINYINT] NULL,
	CONSTRAINT FK_BILL FOREIGN KEY ([BillID]) REFERENCES [dbo].[Bill]([BillID]),
	CONSTRAINT FK_FOOD_2 FOREIGN KEY ([FoodID]) REFERENCES [dbo].[Food]([FoodID]),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO